import { useEffect, useState, useRef } from "react";

const API = (endpoint, DTO, setApiResponse, fetchAllowed, fetchOnce) => {
  const isMounted = useRef(null);
  const [initialFetch, setInitialFetch] = useState(true)

  if(fetchAllowed && initialFetch && initialFetch)
    if(fetchOnce)
      setInitialFetch(false)

    fetch(endpoint, DTO===null ? null : { 
        method: "POST",
        body: JSON.stringify(DTO),
        headers:{
          'Content-Type': 'application/json'
        }
      } 
    )
    .then((response)=>{
        return response.json()
    })
    .then(response => {
      if(isMounted.current){
          setApiResponse(response)
      }  
    })
    .catch(err => console.log(err))

    useEffect(() => {
      isMounted.current = true;
      return () => {
        isMounted.current = false;
      }
    }, []);
}
export default API;