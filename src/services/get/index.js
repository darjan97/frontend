const Service= (url) =>{
  return fetch(url)
    .then(resp => resp.json())
}

export default Service;