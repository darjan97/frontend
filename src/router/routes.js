export const homeRoute = '/'
export const aboutRoute = '/about'
export const createPostRoute= "/createPost"
export const viewPostRoute= "/viewPost"
export const registerRoute= "/register"
export const loginRoute= "/login"