import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import Home from "../pages/home"
import About from "../pages/about"
import CreatePost from "../pages/createPost";
import ViewPost from "../pages/viewPost";
import Login from "../pages/login";
import Register from "../pages/register";

import * as routes from "./routes";

const Router = () => {
    
    return(
        <BrowserRouter>
            <Route exact path={routes.homeRoute} render={() => <Home /> }/> 
            <Route path={routes.aboutRoute} render={()=><About />} /> 
            <Route path={routes.createPostRoute} render={()=><CreatePost />}/> 
            <Route path={routes.viewPostRoute} render={()=><ViewPost />}/>
            <Route path={routes.loginRoute} render={()=><Login />}/>  
            <Route path={routes.registerRoute} render={()=><Register />}/> 
            {/* <Route  path={orderDetailsRoute} render={(props)=><OrderDetails state={props.location.state}/>} />  */}
        </BrowserRouter>
    )
}

export default Router