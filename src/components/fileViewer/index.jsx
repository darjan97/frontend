import React, { createRef, useState, Fragment } from "react";
import Button from '@material-ui/core/Button';
// import MultiReader from "./multiReader"
import PDFReader from "./pdfReader";
import AttachFileIcon from '@material-ui/icons/AttachFile';
import classStyles from './styles';

const FileViewer = () => {
    const classes = classStyles();
    const inputRef = createRef();
    const [ pdfFiles, setPdfFiles ] =  useState([]);
    const [ docxFiles, setDocxFiles ] =  useState([]);

    const addFile = (files) => {

        const selectedPdfFiles = [];
        const selectedDocxFiles = [];


        for(let i=0; i < files.length; i++){
                const file = files[i]

                const fileName = file.name.split(".");
                const fileExtension = fileName[fileName.length-1]
    
                if(fileExtension==="pdf")
                    selectedPdfFiles.push(file)
                else if(fileExtension==="docx" || fileExtension==="doc")
                    selectedDocxFiles.push(file)
                else
                    alert("Uneli ste nevalidan fajl"); 

        }

        if(selectedPdfFiles.length !== 0)
            setPdfFiles(selectedPdfFiles)

        if(selectedDocxFiles.length !== 0)
            setDocxFiles(selectedPdfFiles)
    }


    return (
        <Fragment>
            <Button className={classes.button} onClick={() => inputRef.current.click()} >
            <AttachFileIcon/>
                Dodaj prilog...
            </Button>
            <input 
            type="file" 
            ref={inputRef} 
            style={{display: "none"}} 
            onChange={(e) => addFile(e.target.files)} 
            // accept=".pdf, .doc*"
            accept=".pdf"
            multiple="multiple"
            />
            {/* <MultiReader docxFiles={docxFiles}/> */}
            <PDFReader pdfFiles={pdfFiles}/> 
        </Fragment>
    )

}

export default FileViewer

