import React, { Fragment } from "react";
import FileViewer from 'react-file-viewer';

const MultiReader = (props) => {
    const { docxFiles } = props
    const url = URL.createObjectURL(docxFiles[0])
    // fileExtension==="docx" || fileExtension==="doc"

    return (
        <Fragment>
        {
            docxFiles.length !== 0 ?
            <FileViewer 
            // fileType={extension} 
            filePath={url} 
            />
            : 
            null
        }
            <a href={this.props.url} download>Click to download</a>
        </Fragment>
    );
}

export default MultiReader