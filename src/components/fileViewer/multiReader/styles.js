import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    container: {
        flexGrow: 1,
        display: "flex",
        backgroundColor: "#555c640d",
    },
    
    contentContainer: {
        display: "flex",
        flexDirection: "column",
        width: "80%",
        minWidth: 250,
        "@media (max-width: 650px)" : {
            width: "90%",
        }
    },

    paper: {
        padding: 10,
        marginBottom: 30,
        minHeight: 150,
        // display: "flex",
        // flexDirection: "column",
        border: "0.5px solid #c9cace",
    },
});

export default classStyles;
