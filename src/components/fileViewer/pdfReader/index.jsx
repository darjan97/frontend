import React, { createRef, useState, useEffect, Fragment } from "react";
import { Document, Page, pdfjs} from 'react-pdf';
import CircularProgress from '@material-ui/core/CircularProgress';
import classStyles from './styles';
import Divider from '@material-ui/core/Divider';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {useSelector} from "react-redux"

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const PDFReader = (props) => {
    const classes = classStyles();
    const { pdfFiles } = props;
    const [ numberOfPages, setNumberOfPages ] = useState(0); 
    const [ width, setWidth ] = useState();
    const screen = useSelector(state => state.screen);
    const { screenWidth } = screen;
    const [selectedFile, setSelectedFile] = useState(0);
    const [pages, setPages] = useState([])
    const parentRef = createRef();
    
    useEffect(() => {
        setWidth(parentRef.current.offsetWidth)
    }, [width, screenWidth]);

    const changeSelectedFile = (event) => {
        setPages([])
        setSelectedFile(event.target.value);
    };

    useEffect(() => {
        if(numberOfPages!==0){
            let pagesArray = [];
            for(let i=1; i<=numberOfPages; i++)
                pagesArray.push(i);

            setPages(pagesArray);
        }
    }, [numberOfPages]);


    return (
        <div ref={parentRef}>
        {   
            pdfFiles.length !== 0 ?
            <Fragment>
                {
                pdfFiles.length > 1 ?
                <FormControl className={classes.formControl} fullWidth>
                <InputLabel >Odaberi materiajl</InputLabel>
                <Select
                value={selectedFile}
                onChange={changeSelectedFile}
                >
                <MenuItem value={0}>Prosek na SIP-u</MenuItem>
                <MenuItem value={1}>Magnetizam</MenuItem>
                </Select>
                </FormControl>
                :
                <Divider className={classes.divider}/>
                }

                <div className={classes.position} >
                    <Document 
                    file={pdfFiles[selectedFile]}
                    onLoadSuccess={({ numPages }) => setNumberOfPages(numPages)}
                    loading={<CircularProgress/>}
                    className={classes.document}
                    >
                    {   
                        pages.map(pageNumber => 
                            <Page 
                            className={classes.page} 
                            width={width} 
                            loading="" 
                            pageNumber={pageNumber} 
                            key={pageNumber} 
                        />)
                    }
                    </Document>
                    <Divider className={classes.divider}/>
                </div>
            </Fragment>
            : 
            null
        }
        </div>
    );
}

export default PDFReader;