import React from 'react';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import Typography from '@material-ui/core/Typography';
import { CLEAR_TOKEN } from "../../../../store/user/actions";
import {useDispatch} from "react-redux"

export const Logout = () => {
  const classes = classStyles();
  const dispatch = useDispatch(); 

  const handleClick = () => {
    dispatch({type: CLEAR_TOKEN});
  }

  return(
    <Button className={classes.color} onClick={handleClick}>
      <Typography variant="subtitle2">
        Odjavljivanje
      </Typography>
    </Button>
  )
}

export default Logout;
