import Service from "./services/get";

const url = 'https://api.github.com/users/hadley/orgs'

const Provider = async () => {
    try {
        const resultList = await Service(url)
        return resultList
    } catch (err) {
        // throw new Error('Go boom')
    }
}

export default Provider;