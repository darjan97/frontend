import React, { useState, useContext, useEffect } from 'react';
import Button from "@material-ui/core/Button";
import { withRouter } from 'react-router-dom';
import { homeRoute } from "../../config/Routes";
import classStyles, { styles } from './styles';
import { SCREEN_WIDTH } from "./actions";
import {useSelector, useDispatch} from "react-redux"


export const Hook = (props) => {
    const [ value, setValue ] = useState(false); 
    const classes = classStyles(value);
    
    const home = useSelector(state => state.home);
    const { hideLeftBar, departments: optionsStore } = home;
    const dispatch = useDispatch(); 

    useEffect(() => {
        return () => {setValue(true)}
    }, []);

    const handleClick = () => {
        dispatch({type: SCREEN_WIDTH, payload: 1000});

        const path = props.location.pathname
        if(path!==homeRoute)
            props.history.push(homeRoute);
    }

    return (
        <div>
            <h1>{state.posts}</h1>
            <Button className={classes.color} style={styles.buttonColor("red")}  onClick={handleClick}/>
            <Button className={classes.colorCondition} onClick={() => setValue(!value)}></Button>
            {/* className={classes.colorCondition} //condition is: const classes = classStyles(value);*/}
        </div>
    )
}

export default withRouter(Hook);





