import { thenApiCall } from "./service";

const Provider = async () => {
    try {
        const resultList = await thenApiCall()
        return resultList
    } catch (err) {
        throw new Error('Go boom')
    }
}

export default Provider;