import React, { useState, useEffect, useRef } from 'react';
import Provider from "./Provider";
import {ApiCallThenCatch} from "./service"

function App() {
  const [error, setError] = useState(null)
  const isMounted = useRef(null);
  const [jsonState, setJsonState] = useState(null)

  useEffect(() => {
    isMounted.current = true;
    return () => { isMounted.current = false;}
  }, []);

  useEffect(() => {
    Provider()
      .then(setJsonState)
      .catch(() => setError("some thing went wrong..."))
  }, [])

//   const localProvider = async () => {
//     try {
//       const resultList = await ApiCallThenCatch()
//       setJsonState(resultList)
//     } catch (err) {
//       setError('Something went wrong..')
//     }
//   }

//   useEffect(() => {
//     localProvider()    
//   }, [])

  if (error) return <h1>{error}</h1>

  return jsonState ? JSON.stringify(jsonState) : <h1>Waiting for data...</h1>
}

export default App;
