const url = 'https://api.github.com/users/hadley/orgs'

//VRACA PROMIS
export async function ApiCallAsyncAwait () {
    const resp = await fetch(url)
    const respJson = await resp.json()
    return respJson
}

//NE VRACA PROMIS
export function ApiCallThenCatch () {
    return fetch(url)
      .then(resp => resp.json())
      
      // fetch(endpoint, DTO===null ? null : { 
      //   method: "POST",
      //   body: JSON.stringify(DTO),
      //   headers:{
      //     'Content-Type': 'application/json'
      //   }
      // } 
      // )
      // .then((response)=>{
      //   return response.json()
      // })
}
  