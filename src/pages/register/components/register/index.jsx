import React, { Fragment, useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import RegisterAPI from "../registerAPI"
import Dialog from "../dialog";
import { homeRoute } from "../../../../router/routes";
import { withRouter } from 'react-router-dom';
import { SET_TOKEN } from "../../../../store/user/actions";
import {useDispatch} from "react-redux"

const Register = (props) => {
  const classes = classStyles();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [dialog, setDialog] = useState(false);
  const [dialogTitle, setDialogTitle] = useState("");
  const [dialogContet, setDialogContent] = useState("");
  const [apiResult, setApiResult] = useState(null);
  const [registerButton, disableRegisterButton] = useState(false);
  const dispatch = useDispatch(); 

  const onClick = () => {

    if(password!==passwordConfirm){
      setDialogTitle("Sifre se razlikuju")
      setDialogContent("Pokusajte ponovo.")
      setDialog(true);
    }
    else{
      disableRegisterButton(true)

      let DTO = {
        username: username, 
        password: password
      }
   
      RegisterAPI(DTO)
      .then(setApiResult)
    }
  }

  useEffect(() => {
    if(apiResult!=null){
      const {status} = apiResult;

      if(status=="error"){
        const {data} = apiResult;
        const {code} = data;

        if(code=="ER_DUP_ENTRY"){
          setDialogTitle("Korisnicko ime vec postoji")
          setDialogContent("Pokusajte ponovo.")
          setDialog(true);
        }else{
          setDialogTitle("Desila se nepoznata greska")
          setDialogContent("Posaljite ovu gresku tehnickoj podrsci: "+code)
          setDialog(true);
        }
      }

      if(status=="sucess"){
        disableRegisterButton(false)
        dispatch({type: SET_TOKEN, payload: username});
        props.history.push(homeRoute);
      }
    }

    disableRegisterButton(false)
  }, [apiResult]);

  return (
    <div className={classes.container}>
      <Typography variant="h4" className={classes.heading} >Registracija</Typography>
      <Paper className={classes.paper} elevation={0}>
      <TextField
          placeholder="Korisničko ime"
          autoComplete="username"
          InputProps={{
            className: classes.input
          }}
          fullWidth
          variant="outlined"
          onChange={(e)=>setUsername(e.target.value)}
      />
      <TextField
      placeholder="Šifra"
      type="password"
      InputProps={{
        className: classes.input
      }}
      fullWidth
      variant="outlined"
      onChange={(e)=>setPassword(e.target.value)}
      />
      <TextField
      placeholder="Potvrdi šifru"
      type="password"
      InputProps={{
        className: classes.input
      }}
      fullWidth
      variant="outlined"
      onChange={(e)=>setPasswordConfirm(e.target.value)}
      />
      <Button disabled={registerButton} fullWidth size="large" className={classes.button} onClick={onClick}>Registruj me</Button>
      </Paper>
      <Dialog  dialog={dialog} setDialog={setDialog} dialogTitle={dialogTitle} dialogContet={dialogContet}/>
    </div>
  );
}

export default withRouter(Register);
