import React, { Fragment } from 'react';
import Spacer from "../../../../components/spacer"
import Typography from '@material-ui/core/Typography';
import classStyles from './styles';
import CreateIcon from '@material-ui/icons/Create';
import Button from "@material-ui/core/Button";
import SvgIcon from '@material-ui/core/SvgIcon';
import OutlookIcon from "../../../../assets/Outlook.png";
import { Icon } from "@material-ui/core";
import IconButton from '@material-ui/core/IconButton';

const Register = () => {
  const classes = classStyles();

  return (
    <Button size="large" className={classes.button}>
      <img src={OutlookIcon} height={30} width={20} className={classes.image}/>
      Registruj me
    </Button>
  );
}

export default Register;
