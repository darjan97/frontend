import React, { Fragment } from 'react';
import Spacer from "../../../../components/spacer"
import Typography from '@material-ui/core/Typography';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import PersonIcon from '@material-ui/icons/Person';
import Paper from '@material-ui/core/Paper';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';

const Login = () => {
  const classes = classStyles();

  return (
    <div className={classes.container}>
      <Typography variant="h4" className={classes.heading} >Prijavljivanje</Typography>
      <Paper className={classes.paper} elevation={0}>
      <TextField
          placeholder="Korisničko ime"
          autoComplete="username"
          InputProps={{
            className: classes.input
          }}
          fullWidth
          variant="outlined"
        />
        <TextField
        placeholder="Šifra"
        type="password"
        autoComplete="current-password"
        InputProps={{
          className: classes.input
        }}
        fullWidth
        variant="outlined"
      />
      <Button fullWidth size="large" className={classes.button}>Prijavi me</Button>
      <Link href="#" className={classes.passwordChange} color="inherit" underline="always">Ne sećаš se lozinke?</Link>

      </Paper>
    </div>
  );
}

export default Login;

// Deljenje znanja predstavlja osnovni čin prijateljstva. Zato što je to način da nešto date, bez da bilo šta izgubite. - Richard Stallman
