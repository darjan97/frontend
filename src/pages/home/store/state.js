const initialState = {
    posts: [],
    years: [],
    departments: [],
    error: null,
    hideLeftBar: true,
};

const state = (() => {
    
    let state = JSON.parse(localStorage.getItem("home"));
    return localStorage.getItem("home") !== null ? JSON.parse(localStorage.getItem("home")) : initialState
}
)()

export default state