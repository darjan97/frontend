export const yearOptions = [
    {key: 0, name: "First"},
    {key: 1, name: "Second"},
    {key: 2, name: "Third"},
    {key: 3, name: "Fourth"}
]

export const departmentOptions = [
    {key: 0, name: "Computer science"},
    {key: 1, name: "Robotics"},
    {key: 2, name: "Electronic"},
    {key: 3, name: "Micro electronic"},
    {key: 4, name: "Energetic"},
    {key: 5, name: "Telecomuniaction"}
]