import * as actions from "./actions";
import { yearOptions, departmentOptions } from "./mocks";
import initialState from "./state"

const Reducer = (state=initialState, action) => {

    switch (action.type) {
        case actions.HOME_SET_POSTS:
            return {
                ...state,
                posts: action.payload
            };
        case actions.HOME_HIDE_LEFT_BAR:
        return {
            ...state,
            hideLeftBar: action.payload
        };
        case actions.HOME_ADD_YEARS:
            return {
                ...state,
                years: state.years.concat(yearOptions.filter(option => option.key === action.payload)[0])
            };
        case actions.HOME_REMOVE_YEARS:
            return {
                ...state,
                years: state.years.filter(option => option.key !== action.payload)
            };
        case actions.HOME_ADD_DEPARTMENTS:
            return {
                ...state,
                departments: state.departments.concat(departmentOptions.filter(option => option.key === action.payload)[0])
            };
        case actions.HOME_REMOVE_DEPARTMENTS:
            return {
                ...state,
                departments: state.departments.filter(option => option.key !== action.payload)
            };    
        default:
            return state;
    }
  };
  
  export default Reducer;