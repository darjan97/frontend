import React, { Fragment } from 'react';
import Button from '@material-ui/core/Button';
import classStyles from './styles';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import { withRouter } from 'react-router-dom';
import { createPostRoute } from "../../../../router/routes";
import ArrowForward from '@material-ui/icons/ArrowForward';

const Controll = (props) => {
  const classes = classStyles();

  return (
    <Fragment>
      <div className={classes.container}>
        <Typography variant="h6" color="inherit" className={classes.fontType}>Postavi materijal na oglasnu tablu</Typography>
        <ArrowForward/>
        <Button
          onClick={() => props.history.push(createPostRoute)}
          className={classes.button}
        >
          <Typography variant="button" color="inherit" className={classes.fontStyle}>Kreiraj objavu</Typography>
        </Button>

      </div>
      <Divider className={classes.divider}/>
    </Fragment>
    );
}

export default withRouter(Controll);