import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    button: {
        backgroundColor: "#0099feba",
        color: "white",
        border: "1px solid black",
        '&:hover': {
            boxShadow: "0px 1px 1px 0px rgba(0,0,0,0.75)",   
            backgroundColor: "#0099fee3",
        },
        marginRight: 5,
        marginBottom: 8,
        minWidth: 116,
        // width: 200,
    },

    fontType: {
        textTransform: 'none',
    },
});

export default classStyles;
