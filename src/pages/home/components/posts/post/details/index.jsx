import React from 'react';
import Typography from '@material-ui/core/Typography';
import classStyles from './styles';
import Spacer from "../../../../../../components/spacer";
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Today from '@material-ui/icons/Today';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { viewPostRoute } from "../../../../../../router/routes";

const Details = (props) => {
  const classes = classStyles();
  const { likes } = props;

  return (
    <div className={classes.controlls}>
      {/* <ThumbUpIcon/>
      <Typography color="textSecondary" className={classes.like}>  { likes } </Typography>  */}
      <Today className={classes.date}/>
      <Typography>  24.03.2020 </Typography> 
      <Spacer/>
      <Button size="small" className={classes.backgroundColor} onClick={() => props.history.push(viewPostRoute)}>
        <Typography variant="button" color="inherit" className={classes.fontType}>Prikaži objavu</Typography>
      </Button>
    </div>
  );
}

export default withRouter(Details);