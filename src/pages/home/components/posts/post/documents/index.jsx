import React, { Fragment } from 'react';
import classStyles from './styles';
import Typography from '@material-ui/core/Typography';
import PDF from "../../../../../../assets/pdf1.png"

const Document = (props) => {
  const classes = classStyles();
  const { documents } = props;

  return (
    documents.length === 0 ?
    null
    :
    <div className={classes.container}>
      {
        documents.map((document, index) => (

          <div key={index}>  
            <img src={PDF} alt="Smiley face" height="30" width="30" className={classes.photo}/>
            <Typography className={classes.text}>
              { document }
            </Typography> 
          </div>
        ))
      }

    </div>
  );
}

export default Document;