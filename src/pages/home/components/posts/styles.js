import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    container: {
        flexGrow: 1,
    },

});

export default classStyles;
