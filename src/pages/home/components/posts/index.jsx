import React from 'react';
import classStyles from './styles';
import Post from "./post";
import SIP from "../../../../assets/SIP.jpg"
import Account from "../../../../assets/Account.jpg"

const Posts = () => {
  const classes = classStyles();

  const data1 = {
    title: "Kako da pogledam svoj prosek na sipu?",
    text: `Student sam prve godine i interesuje me kako
          mogu da pogledam svoj prosek? Na salteru su mi rekli 
          da pogledam na SIPu, ali nisam uspeo da se snadjem...`,
    images: [],
    documents: [],
    type: "q",
    tags: ["Četvrta", "Računarstvo i informatika", "Informacioni sistemi"],
    likes: 50, 
  }

  const data2 = {
    title: "Kako da pogledam svoj prosek na sipu?",
    text: "",
    images: [SIP, Account, SIP],
    documents: [],
    type: "q",
    tags: ["Četvrta", "Računarstvo i informatika", "Informacioni sistemi"],
    likes: 25,
  }
  
  const data3 = {
    title: "Uputstvo kako pogledati prosek na SIP-u.",
    text: "",
    images: [],
    documents: ["Prosek na SIP-u.pdf", "Magnetizam.pdf"],
    type: "m",
    tags: ["Četvrta", "Računarstvo i informatika", "Informacioni sistemi"],
    likes: 100,
  }

  const data4 = {
    title: "Kako da pogledam svoj prosek na sipu?",
    text: `Student sam prve godine i interesuje me kako
          mogu da pogledam svoj prosek? Na salteru su mi rekli 
          da pogledam na SIPu, ali nisam uspeo da se snadjem...`,
    images: [SIP, Account, SIP],
    documents: ["Prosek na SIP-u.pdf", "Magnetizam.pdf"],
    type: "q",
    tags: ["Četvrta", "Računarstvo i informatika", "Informacioni sistemi"],
    likes: 50, 
  }

  return (
    <div className={classes.container}>
      <Post data={data1}/>
      <Post data={data2}/>
      <Post data={data3}/>
      <Post data={data4}/>
    </div>
  );
}

export default Posts;