import React, { useContext, useState } from 'react';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import { HOME_SET_POSTS } from "../../store/actions";
import {useSelector, useDispatch} from "react-redux"

const Search = () => {
  const classes = classStyles();
  const [choices, setChoices] = useState([])
  const dispatch = useDispatch(); 

  const handleSearch = () => {
    dispatch({type: HOME_SET_POSTS, payload: choices});
  }

  return (
    <Paper component="form" className={classes.root}>
     <InputBase
        className={classes.input}
        placeholder="Pretraga"
      />
      <Button className={classes.searchButton}  onClick={handleSearch}><SearchIcon /></Button>
    </Paper>
  );
}

export default Search