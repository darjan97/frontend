import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    input: {
        width: "100%",
        marginLeft: 5   ,
    },

    searchButton: {
        flexGrow: 1,
        backgroundColor: "#0099feba",
        borderRadius: 0,
        color: "white",
        '&:hover': {
          backgroundColor: "#0099fee3",
      },
    },

    root: {
        display: 'flex',
        flexGrow: 1,
        height: 40,
        boxShadow: "0px 0px 1px 1px rgba(0,0,0,0.75)",
        marginBottom: 10,
      },
});

export default classStyles;
