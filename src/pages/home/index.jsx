import React, { Fragment } from 'react';
import Category from "./components/category";
import Posts from "./components/posts";  
import Filters from "./components/filters";
import CreatePost from "./components/createPost";
import Search from "./components/search";
import Spacer from "../../components/spacer";
import classStyles from './styles';
import Navbar from "../../components/navbar";
import Leftbar from "./components/leftBar";

const Home = () => {
    const classes = classStyles();

    return (
    <Fragment>
    <Navbar/>
    <div className={classes.rootContainer}>
      <Leftbar/>
       <Spacer/>
        <div className={classes.contentContainer}>
          <CreatePost/>
          <Search/>
          <Filters/>
          {/* <Category/> */}
          <Posts/>
        </div>        
        <Spacer/>
    </div>        
    </Fragment>
  );
}

export default Home;