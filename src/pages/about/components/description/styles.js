import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    container: {
        backgroundColor: "#282828",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        flexGrow: 1,
    },

    descriptionContainer: {
        color: "white",
        textAlign: "center",
    },

    position: {
        textAlign: "center",
        height: "60%",
        marginTop: "50px",
    },
    
});

export default classStyles;
