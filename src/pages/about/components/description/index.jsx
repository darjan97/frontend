import React, { Fragment } from 'react';
import classStyles from './styles';
import image from "../../../assets/GraduationCap.jpg"
import Typography from '@material-ui/core/Typography';
import { aboutRoute } from "../../../../router/routes";
import { withRouter } from 'react-router-dom';


const Description = (props) => {
  const classes = classStyles();

  return (
      <Fragment>
          <div className={classes.position}>
            <img src={image} alt="Smiley face" height="300" width="300"/>
        </div>
        <div className={classes.container}>
            {props.location.pathname !== aboutRoute ? null :
                 <Typography variant="h6" className={classes.descriptionContainer}>    
                     <br/> 
                     Sajt je nastao kako bi se olakšala razmena rešenih blanketa, 
                     skripti i ostalog materijala među studentima. 
                 </Typography>
            }
        </div>
      </Fragment>
    );
}

export default withRouter(Description);