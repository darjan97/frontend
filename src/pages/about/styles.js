import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    container: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "column",
    },
});

export default classStyles;
