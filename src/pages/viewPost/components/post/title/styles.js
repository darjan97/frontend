import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({

    container: {
        marginBottom: 20,
        marginTop: 10,
    },

});

export default classStyles;
