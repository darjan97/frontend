import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import classStyles from './styles';
import Spacer from "../../../../../components/spacer";
  
const Post = () => {
  const classes = classStyles();

  return (
    <Fragment>
      {/* <div className={classes.container}> */}
      <Typography variant="h6" className={classes.container}>
        Kako da pogledam svoj prosek na sipu?
      </Typography>
      {/* </div> */}
      <Spacer/>
    </Fragment>
  );
}

export default Post;