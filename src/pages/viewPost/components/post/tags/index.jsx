import React from 'react';
import classStyles from './styles';
import Chip from '@material-ui/core/Chip';

const Post = () => {
  const classes = classStyles();

  return (
    <div>
      <Chip label="Četvrta" variant="outlined" className={classes.tag}/>
      <Chip label="Računarstvo i informatika" variant="outlined" className={classes.tag}/>
      <Chip label="Informacioni sistemi" variant="outlined" className={classes.tag}/>
    </div>
  );
}

export default Post;