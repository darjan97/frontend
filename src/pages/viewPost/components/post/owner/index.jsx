import React from 'react';
import Typography from '@material-ui/core/Typography';
import classStyles from './styles';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Account from "../../../../../assets/Account.jpg"
import Avatar from '@material-ui/core/Avatar';
import Link from '@material-ui/core/Link';
import Spacer from "../../../../../components/spacer";
import HelpIcon from '@material-ui/icons/Help';
import Book from '@material-ui/icons/MenuBook';

const Owner = (props) => {
  const classes = classStyles();

  const type = "q"

  const preventDefault = event => event.preventDefault();

  return (
    <div className={classes.controlls}>
      <Button  className={classes.button} disableRipple>
        <Avatar alt="Smiley face" src={Account}/>
      </Button>
      <Typography>  
        <Link href="#" onClick={preventDefault} color="inherit">
          Marko Marković 
        </Link>
      </Typography>
      <Spacer/>
      { type === "q" ?
        <HelpIcon className={classes.postType} />
        :
        <Book className={classes.postType} />
      }
    </div>
  );
}

export default withRouter(Owner);