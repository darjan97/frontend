import React, { Fragment } from 'react';
import classStyles from './styles';
import PDFReader from "../../../../../components/fileViewer/pdfReader";
import PDFFile from "../../../../../assets/uputstvo.pdf";
import Divider from '@material-ui/core/Divider';

const Post = () => {
  const classes = classStyles();

  // <a href={this.props.url} download>Click to download</a>
  // const url = URL.createObjectURL(file)

  return (
    <Fragment>
      <Divider className={classes.divider}/>
      <PDFReader extension="pdf" file={PDFFile}/> 
      <a href={PDFFile} download>Preuzmi materijal</a>
    </Fragment>
  );
}

export default Post;