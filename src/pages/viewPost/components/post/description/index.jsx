import React from 'react';
import classStyles from './styles';
import Typography from '@material-ui/core/Typography';
import SIP from "../../../../../assets/SIP.jpg"

const Post = () => {
  const classes = classStyles();

  return (
    <Typography className={classes.text}>
      Student sam prve godine i interesuje me kako
      mogu da pogledam svoj prosek? Na salteru su mi rekli 
      da pogledam na SIPu
      <br/>
      <br/>
      <img src={SIP} alt="Smiley face" height="300" className={classes.photo}/>
      <br/>
      <br/>
      , ali nisam uspeo da se snadjem.
    </Typography>
  );
}

export default Post;