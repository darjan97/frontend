import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
   
    photo: {
        border: "solid 1px",
        maxWidth: 400,
        width: "100%",
    },

    text: {
        marginBottom: 20,
    }

});

export default classStyles;
