import React from 'react';
import Paper from '@material-ui/core/Paper';
import classStyles from './styles';
import Title from "./title";
import Details from "./details";
import Tags from "./tags";
import Owner from "./owner";
import Description from "./description"
import Divider from '@material-ui/core/Divider';
import Document from "./document";

const Post = () => {
  const classes = classStyles();

  return (
    <Paper className={classes.paper} elevation={0}>
      <Owner/>
      <Title/>
      <Description/>
      {/* <Document/> */}
      <Tags/>
      <Details/>
    </Paper>
  );
}

export default Post;