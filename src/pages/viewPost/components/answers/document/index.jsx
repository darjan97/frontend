import React, { Fragment } from 'react';
import classStyles from './styles';
import PDFReader from "../../../../../components/fileViewer/pdfReader";
import PDFFile from "../../../../../assets/uputstvo.pdf";

const Post = () => {
  const classes = classStyles();

  // <a href={this.props.url} download>Click to download</a>
  // const url = URL.createObjectURL(file)

  return (
    <Fragment>
      <PDFReader pdfFiles={[PDFFile]}/> 
      <a href={PDFFile} download>Preuzmi materijal</a>
    </Fragment>
  );
}

export default Post;