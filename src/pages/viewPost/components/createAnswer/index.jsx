import React from 'react';
import Paper from '@material-ui/core/Paper';
import classStyles from './styles';
import Title from "./formTitle";
import Description from "./description";
import FileViewer from "../../../../components/fileViewer"
import Controlls from "./controlls";

const CreateAnswer = () => {
  const classes = classStyles();

  return (
    <Paper className={classes.paper} elevation={0}>
      <Title/>
      <Description/>
      <FileViewer/>
      <Controlls/>
    </Paper>
  );
}

export default CreateAnswer;