import React, { Fragment } from 'react';
import Navbar from "../../components/navbar";
import classStyles from './styles';
import Spacer from "../../components/spacer";
import Post from "./components/post";
import Answers from "./components/answers";
import CreateAnswer from "./components/createAnswer"

const ViewPost = () => {
  const classes = classStyles();

  return (
    <Fragment>
      <Navbar/>
      <div className={classes.container}>
        <Spacer/>
        <div className={classes.contentContainer}>
          <Post/>
          <Answers/>
          <CreateAnswer/>
        </div>        
        <Spacer/>
      </div> 
    </Fragment>
    );
}

export default ViewPost;