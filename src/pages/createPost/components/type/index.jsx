import React from 'react';
import classStyles from './styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const Type = () => {
    const classes = classStyles();
  const [age, setAge] = React.useState('');

  const handleChange = event => {
    setAge(event.target.value);
  };

    return (
        <FormControl className={classes.formControl}>
          <InputLabel >Tip</InputLabel>
          <Select
            value={age}
            onChange={handleChange}
          >
            <MenuItem value={10}>Pitanje</MenuItem>
            <MenuItem value={20}>Materijal</MenuItem>
          </Select>
      </FormControl>
  );
}

export default Type;