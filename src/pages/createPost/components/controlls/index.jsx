import React from 'react';
import classStyles from './styles';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import { homeRoute } from "../../../../router/routes";

const Controlls= (props) => {
    const classes = classStyles();

    return (
      <div className={classes.container}>
      <Button
      // onClick={() => props.history.push(askQuestionRoute)}
      className={classes.submit}
      >
        <Typography variant="button" color="inherit" className={classes.fontStyle}>Potvrdi</Typography>
      </Button>

      <Button
      onClick={() => props.history.push(homeRoute)}
      className={classes.cancel}
      >
        <Typography variant="button" color="inherit" className={classes.fontStyle}>Odustani</Typography>
      </Button>
      </div>
     
  );
}

export default withRouter(Controlls);