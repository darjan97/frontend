import React from 'react';
import classStyles from './styles';
import Slider from "../components/slider"
import Year from "../components/year";
import Department from "../components/department";
import Divider from '@material-ui/core/Divider';
import {useSelector} from "react-redux"

const LeftBar = () => {
    const createPost = useSelector(state => state.createPost);
    const screen = useSelector(state => state.screen);
    const { screenWidth } = screen;
    const { hideLeftBar } = createPost;
    const classes = classStyles(hideLeftBar);  

    return (
        screenWidth <= 650 ? null 
        :
        <div className={classes.filtersContainer}>
            <div className={classes.slide}>
                <Slider />
                <Divider className={classes.deviderColor}/>
                <Year/>
                <Department/>
            </div>
        </div>
    );
}

export default LeftBar;