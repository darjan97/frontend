import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
  
    color: (hidden) => ({
        transform: !hidden ? "rotate(180deg)" : "rotate(0deg)",
    }),

    deviderColor: {
        backgroundColor: "white",
    },

    filtersContainer: (hidden) => ({
        position: "relative",
        overflow: "hidden",
        minWidth: hidden ? 80 : "200px",
        transition: "0.3s ease-out",
    }),
    
    slide: (hidden) => ({
        position: "fixed",
        left: hidden ? -120 : 0,
        width: "200px",
        transition: "0.3s ease-out",
        backgroundColor: "white",
        textAlign: "right",
        overflow: "auto",
        boxShadow: "2px 0px 5px 0 #393a3d26",
        borderRight : "1px solid #e0e0e0",
        height: "100%",
    }),

});


export default classStyles;
