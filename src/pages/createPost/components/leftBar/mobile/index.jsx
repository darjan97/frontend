import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import { CREATE_POST_HIDE_LEFT_BAR } from "../../../store/actions";
import classStyles from './styles';
import Year from "../components/year";
import Department from "../components/department";
import Logo from "../../../../../components/navbar/components/blog";
import Divider from '@material-ui/core/Divider';
import {useSelector, useDispatch} from "react-redux"

export default function TemporaryDrawer() {
  const classes = classStyles();  
  const screen = useSelector(state => state.screen);
  const createPost = useSelector(state => state.createPost);
  const { screenWidth } = screen;
  const { hideLeftBar } = createPost;
  const dispatch = useDispatch(); 

  const toggleDrawer = event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) 
        {return;}

    dispatch({type: CREATE_POST_HIDE_LEFT_BAR, payload: !hideLeftBar});
  };

  return (
      screenWidth > 650 ? null 
        :
      <Drawer open={!hideLeftBar} onClose={toggleDrawer} classes={{paper: classes.drawer}}>
        <Logo/>
        <Divider className={classes.deviderColor}/>
        <Year/>
        <Department/>
      </Drawer>
  );
}
