import React, { Fragment } from 'react';
import DoubleArrow from '@material-ui/icons/DoubleArrow';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import { CREATE_POST_HIDE_LEFT_BAR } from "../../../../store/actions";
import {useSelector, useDispatch} from "react-redux"

const LeftBar = (props) => {
    const createPost = useSelector(state => state.createPost);
    const { hideLeftBar } = createPost;
    const dispatch = useDispatch(); 

    const classes = classStyles(hideLeftBar);  

    const onClick = () => {
        dispatch({type: CREATE_POST_HIDE_LEFT_BAR, payload: !hideLeftBar})
    } 

    return (
        <Fragment>
            <Button fullWidth className={classes.color} disableRipple onClick={onClick}>
                <DoubleArrow className={classes.iconStyle}/>
            </Button>
        </Fragment>
    );
}

export default LeftBar;