import React from 'react';
import ListHeading from "../components/listHeading";
import ListItem from "../components/listItem";
import { CREATE_POST_ADD_YEARS, CREATE_POST_REMOVE_YEARS } from '../../../../store/actions';
import School from '@material-ui/icons/School';
import {useSelector, useDispatch} from "react-redux"

export const optionsView = [
  {key: 0, name: "Prva"},
  {key: 1, name: "Druga"},
  {key: 2, name: "Treća"},
  {key: 3, name: "Četvrta"}
];

const Year = () => {
  const createPost = useSelector(state => state.createPost);
  const { hideLeftBar, years: optionsStore } = createPost;
  const dispatch = useDispatch(); 

  const handleOptionChange =(key) => {
    if(checked(key))
      dispatch({type: CREATE_POST_REMOVE_YEARS, payload: key});
    else
      dispatch({type: CREATE_POST_ADD_YEARS, payload: key});
  }

  const checked = (key) => {
    // console.log("aaaaaaaaaaaa");
    // console.log(optionsStore)
    return optionsStore.filter(option => option.key===key).length > 0
  }

  return (
    <ListHeading hideLeftBar={hideLeftBar} name={"Godina"} icon={<School/>}>
      {optionsView.map((view, index) => 
        <ListItem 
          name={view.name}  
          checked={checked(view.key)} 
          notify={() => handleOptionChange(view.key)} 
          key={index}
        />
      )}
    </ListHeading>
  );
}

export default Year
