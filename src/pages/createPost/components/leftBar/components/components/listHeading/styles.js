import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
  
    buttonColor: {
        color: "black",
        // color: "white",
        justifyContent: "center",
    },

    left: {
        paddingLeft: "0px",
    },

    arrowColor: {
        // color: "white",
    },

    textColor: {
        // color: "white",
        color: "black",
    },

});


export default classStyles;
