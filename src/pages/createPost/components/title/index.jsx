import React from 'react';
import classStyles from './styles';
import TextField from '@material-ui/core/TextField';

const Title = () => {
    const classes = classStyles();

    return (
      <TextField label="Naslov" className={classes.title} fullWidth/>
  );
}

export default Title;