import React, { Fragment } from 'react';
import Button from "./button";
import classStyles from './styles';
import Tags from "./tags";

export default function ChipsArray() {
  const classes = classStyles();

  return (
    <Fragment>
        <div className={classes.container}>
          <Button/>
          <Tags/>
        </div>
    </Fragment>
  );
}
