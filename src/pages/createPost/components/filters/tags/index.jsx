import React, { Fragment } from 'react';
import Chip from '@material-ui/core/Chip';
import classStyles from './styles';
import {CREATE_POST_REMOVE_YEARS, CREATE_POST_REMOVE_DEPARTMENTS } from '../../../store/actions';
import { optionsView as departmentsView} from "../../leftBar/components/department"
import { optionsView as yearsView} from "../../leftBar/components/year"
import {useSelector, useDispatch} from "react-redux"

export default function ChipsArray() {
  const classes = classStyles();
  const createPost = useSelector(state => state.createPost);
  const { departments, years } = createPost;
  const dispatch = useDispatch(); 

  const handleYearsDelete = key => () => {
    dispatch({type: CREATE_POST_REMOVE_YEARS, payload: key});
  };

  const handleDepartmentsDelete = key => () => {
    dispatch({type: CREATE_POST_REMOVE_DEPARTMENTS, payload: key});
  };

  return (
    <Fragment>
      {yearsView.filter(data => years.some(year => year.key===data.key)).map(data => 
        <Chip key={data.key} label={data.name} onDelete={handleYearsDelete(data.key)} className={classes.chip}/>
      )}

      {departmentsView.filter(data => departments.some(department => department.key===data.key)).map(data => 
        <Chip key={data.key} label={data.name} onDelete={handleDepartmentsDelete(data.key)} className={classes.chip}/>
      )}
    </Fragment>
  );
}
