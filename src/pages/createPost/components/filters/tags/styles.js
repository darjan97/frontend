import { makeStyles } from '@material-ui/styles';

const classStyles = makeStyles({
    chip: {
        margin: "0px 2px 8px 2px",
        backgroundColor: "#0099feba",
        color: "white",
    }

});

export default classStyles;
