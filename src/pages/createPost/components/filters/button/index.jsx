import React, { useContext } from 'react';
import classStyles from './styles';
import Button from "@material-ui/core/Button";
import { CREATE_POST_HIDE_LEFT_BAR } from "../../../store/actions";
import Typography from '@material-ui/core/Typography';
import {useSelector, useDispatch} from "react-redux"

export const Filter = () => {
    const classes = classStyles();
    const createPost = useSelector(state => state.createPost);
    const { hideLeftBar } = createPost;
    const dispatch = useDispatch(); 

    return(
        <Button className={classes.button} size="small" onClick={()=>dispatch({type: CREATE_POST_HIDE_LEFT_BAR, payload: !hideLeftBar})}>
            <Typography variant="button" className={classes.fontType}>Odabrani filteri</Typography>
        </Button>
    )
}

  export default Filter;
