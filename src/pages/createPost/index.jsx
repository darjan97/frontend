import React, { Fragment } from 'react';
import Filters from "./components/filters";
import Spacer from "../../components/spacer";
import Navbar from "../../components/navbar";
import Leftbar from "./components/leftBar";
import classStyles from './styles';
import Title from "./components/title";
import Type from "./components/type";
import FormTitle from "./components/formTitle";
import TextInput from "./components/description";
import Paper from '@material-ui/core/Paper';
import FileViewer from "../../components/fileViewer"
import Controlls from "./components/controlls";

const CreatePost = () => {
    const classes = classStyles();

    return (
    <Fragment>
    <Navbar/>
    <div className={classes.container}>
      <Leftbar/>
      <Spacer/>
      <div className={classes.contentContainer}>
        <FormTitle/>
        {/* <Type/> */}
        <Paper className={classes.paper} elevation={0}>
          <Title/>
          <TextInput/>
          <FileViewer/>
          <Filters/>
          <Controlls/>
        </Paper>
      </div>        
      <Spacer/>
    </div>        
    </Fragment>
  );
}

export default CreatePost;