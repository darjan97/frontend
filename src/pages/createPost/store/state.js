const initialState = {
    posts: [],
    years: [],
    departments: [],
    error: null,
    hideLeftBar: true,
};

export default initialState