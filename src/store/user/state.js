
export const initialState = {
    token: null,
};
  
const state = (() => (
  localStorage.getItem("user") !== null ? 
    JSON.parse(localStorage.getItem("user")) 
    : 
    initialState
))()

export default state