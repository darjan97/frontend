import * as actions from "./actions";
import {initialState} from "./state"; 
 
const Reducer = (state=initialState, action) => {
    switch (action.type) {
        case actions.SET_TOKEN:
            return {
                ...state,
                token: action.payload
            };
        case actions.CLEAR_TOKEN:
            return {
                ...state,
                token: null
            };    
        default:
            return state;
    }
};

export default Reducer;