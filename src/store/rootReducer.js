import screenReducer from "./screen/reducer"
import homeReducer from "../pages/home/store/reducer";
import createPostReducer from "../pages/createPost/store/reducer";
import { combineReducers } from "redux"
import userReducer from "./user/reducer";

const allReducers = combineReducers({
    screen: screenReducer,
    home: homeReducer,
    createPost: createPostReducer,
    user: userReducer,
})

export default allReducers   